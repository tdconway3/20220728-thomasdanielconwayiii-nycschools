package com.example.a20220728_thomasdanielconwayiii_nycschools

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.a20220728_thomasdanielconwayiii_nycschools.databinding.FragmentScoresBinding
import com.example.a20220728_thomasdanielconwayiii_nycschools.model.SchoolsModel
import com.example.a20220728_thomasdanielconwayiii_nycschools.model.ScoresModel
import com.example.a20220728_thomasdanielconwayiii_nycschools.utils.CommonFragment
import com.example.a20220728_thomasdanielconwayiii_nycschools.utils.ResultState

class ScoresFragment : CommonFragment() {

    private val binding by lazy {
        FragmentScoresBinding.inflate(layoutInflater)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        schoolViewModel.mScores.observe(viewLifecycleOwner) {
            when(it) {
                is ResultState.Loading -> {

                }
                is ResultState.Success<*> -> {
                    val score = (it as ResultState.Success<ScoresModel>).result
                    displayScores(score)
                }
                is ResultState.Error -> {
                    showErrorToUser(it.throwable) {

                    }
                }
            }
        }

        return binding.root
    }

    private fun displayScores(score: ScoresModel) {
        binding.mathScore.text = score.satMathAvgScore?.let {
            String.format("Math score: ", it)
        } ?: "Math score no available"
        binding.readScore.text = score.satCriticalReadingAvgScore?.let {
            String.format("Read score: ", it)
        } ?: "Read score no available"
        binding.writingScore.text = score.satWritingAvgScore?.let {
            String.format("Writing score: ", it)
        }  ?: "Writing score no available"
    }
}