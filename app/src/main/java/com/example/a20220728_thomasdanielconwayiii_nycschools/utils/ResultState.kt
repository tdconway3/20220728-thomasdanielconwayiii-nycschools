package com.example.a20220728_thomasdanielconwayiii_nycschools.utils

sealed class ResultState {
    object Loading : ResultState()
    data class Success<T>(val result: T): ResultState()
    data class Error(val throwable: Throwable): ResultState()
}
