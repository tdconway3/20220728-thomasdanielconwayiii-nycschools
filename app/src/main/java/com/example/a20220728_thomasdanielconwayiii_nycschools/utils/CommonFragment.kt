package com.example.a20220728_thomasdanielconwayiii_nycschools.utils

import android.content.Context
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import com.example.a20220728_thomasdanielconwayiii_nycschools.di.NYCApp
import com.example.a20220728_thomasdanielconwayiii_nycschools.viewmodel.ViewModelSchool
import javax.inject.Inject

open class CommonFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    protected val schoolViewModel: ViewModelSchool by activityViewModels { viewModelFactory }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        NYCApp.component.inject(this)
    }

    protected fun showErrorToUser(throwable: Throwable, retryAction: () -> Unit) {
        AlertDialog.Builder(requireActivity())
            .setTitle("Error Occurred")
            .setMessage(throwable.localizedMessage)
            .setPositiveButton("RETRY") { dialog, _ ->
                dialog.dismiss()
                retryAction()
            }
            .setNegativeButton("DISMISS") { dialog, _ ->
                dialog.dismiss()
            }
    }
}