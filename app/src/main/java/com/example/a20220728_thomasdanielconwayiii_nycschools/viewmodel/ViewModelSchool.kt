package com.example.a20220728_thomasdanielconwayiii_nycschools.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.a20220728_thomasdanielconwayiii_nycschools.model.ScoresModel
import com.example.a20220728_thomasdanielconwayiii_nycschools.service.ServiceRepository
import com.example.a20220728_thomasdanielconwayiii_nycschools.utils.ResultState
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.internal.operators.single.SingleCreate
import javax.inject.Inject

class ViewModelSchool @Inject constructor(
    private val serviceRepository: ServiceRepository,
    private val ioScheduler: Scheduler,
    private val compositeDisposable: CompositeDisposable
) : ViewModel() {

    private val _mSchools: MutableLiveData<ResultState> = MutableLiveData(ResultState.Loading)
    val mSchools: LiveData<ResultState> get() = _mSchools

    private val _mScores: MutableLiveData<ResultState> = MutableLiveData(ResultState.Loading)
    val mScores: LiveData<ResultState> get() = _mScores

    var dbn: String? = null

    init {
        retrieveSchools()
    }

    private fun retrieveSchools() {
        serviceRepository.provideSchools()
            .subscribeOn(ioScheduler)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { _mSchools.value = ResultState.Success(it) },
                { _mSchools.value = ResultState.Error(it) }
            )
            .also { compositeDisposable.add(it) }
    }

    fun retrySchoolsCall() {
        retrieveSchools()
    }

    fun retrieveSchoolSAT(dbn: String) {
        serviceRepository.provideScores()
            .subscribeOn(ioScheduler)
            .flatMap { scores ->
                SingleCreate.create<ScoresModel> {
                    scores.firstOrNull { it.dbn == dbn }
                }
            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { _mScores.value = ResultState.Success(it) },
                { _mScores.value = ResultState.Error(it) }
            )
            .also { compositeDisposable.add(it) }
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}