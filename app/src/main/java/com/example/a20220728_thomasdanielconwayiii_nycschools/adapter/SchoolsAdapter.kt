package com.example.a20220728_thomasdanielconwayiii_nycschools.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.a20220728_thomasdanielconwayiii_nycschools.databinding.SchoolItemBinding
import com.example.a20220728_thomasdanielconwayiii_nycschools.model.SchoolsModel

class SchoolsAdapter(
    private val schoolData: MutableList<SchoolsModel> = mutableListOf(),
    private val schoolClicked: (String) -> Unit
) : RecyclerView.Adapter<SchoolViewHolder>() {

    fun updateDataSet(newSchools: List<SchoolsModel>) {
        schoolData.clear()
        schoolData.addAll(newSchools)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolViewHolder =
        SchoolViewHolder(
            SchoolItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ),
            schoolClicked
        )

    override fun onBindViewHolder(holder: SchoolViewHolder, position: Int) =
        holder.bind(schoolData[position])

    override fun getItemCount(): Int = schoolData.size
}

class SchoolViewHolder(
    private val schoolView: SchoolItemBinding,
    private val schoolClicked: (String) -> Unit
) : RecyclerView.ViewHolder(schoolView.root) {

    fun bind(school: SchoolsModel) {

        itemView.setOnClickListener {
            school.dbn?.let {
                schoolClicked(it)
            }
        }
    }
}