package com.example.a20220728_thomasdanielconwayiii_nycschools

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.a20220728_thomasdanielconwayiii_nycschools.adapter.SchoolsAdapter
import com.example.a20220728_thomasdanielconwayiii_nycschools.databinding.FragmentSchoolsBinding
import com.example.a20220728_thomasdanielconwayiii_nycschools.databinding.FragmentScoresBinding
import com.example.a20220728_thomasdanielconwayiii_nycschools.model.SchoolsModel
import com.example.a20220728_thomasdanielconwayiii_nycschools.utils.CommonFragment
import com.example.a20220728_thomasdanielconwayiii_nycschools.utils.ResultState

class SchoolsFragment : CommonFragment() {

    private val binding by lazy {
        FragmentSchoolsBinding.inflate(layoutInflater)
    }

    private val schoolAdapter: SchoolsAdapter by lazy {
        SchoolsAdapter {
            schoolViewModel.dbn = it
            findNavController().navigate(R.id.action_SchoolFrag_to_ScoreFrag)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        binding.schoolRecycler.apply {
            layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            adapter = schoolAdapter
        }

        schoolViewModel.mSchools.observe(viewLifecycleOwner) {
            when(it) {
                is ResultState.Loading -> {

                }
                is ResultState.Success<*> -> {
                    val newSchools = (it as ResultState.Success<List<SchoolsModel>>).result
                    schoolAdapter.updateDataSet(newSchools)
                }
                is ResultState.Error -> {
                    showErrorToUser(it.throwable) {
                        schoolViewModel.retrySchoolsCall()
                    }
                }
            }
        }

        return binding.root
    }
}