package com.example.a20220728_thomasdanielconwayiii_nycschools.service

import com.example.a20220728_thomasdanielconwayiii_nycschools.model.SchoolsModel
import com.example.a20220728_thomasdanielconwayiii_nycschools.model.ScoresModel
import io.reactivex.Single
import javax.inject.Inject

interface ServiceRepository {
    fun provideSchools(): Single<List<SchoolsModel>>
    fun provideScores(): Single<List<ScoresModel>>
}

class ServiceRepositoryImpl @Inject constructor(
    private val serviceApi: ServiceApi
) : ServiceRepository {

    override fun provideSchools(): Single<List<SchoolsModel>> =
        serviceApi.getListOfSchools()

    override fun provideScores(): Single<List<ScoresModel>> =
        serviceApi.getSATScores()

}