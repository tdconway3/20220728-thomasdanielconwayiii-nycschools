package com.example.a20220728_thomasdanielconwayiii_nycschools.service

import com.example.a20220728_thomasdanielconwayiii_nycschools.model.SchoolsModel
import com.example.a20220728_thomasdanielconwayiii_nycschools.model.ScoresModel
import io.reactivex.Single
import retrofit2.http.GET

interface ServiceApi {

    @GET("s3k6-pzi2.json")
    fun getListOfSchools(): Single<List<SchoolsModel>>

    @GET("f9bf-2cp4.json")
    fun getSATScores(): Single<List<ScoresModel>>

    companion object {
        const val BASE_URL = "https://data.cityofnewyork.us/resource/"
    }
}