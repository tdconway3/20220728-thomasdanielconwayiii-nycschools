package com.example.a20220728_thomasdanielconwayiii_nycschools

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.ui.setupActionBarWithNavController
import com.example.a20220728_thomasdanielconwayiii_nycschools.databinding.ActivityMainBinding
import com.example.a20220728_thomasdanielconwayiii_nycschools.di.NYCApp

class MainActivity : AppCompatActivity() {
    private val binding by lazy {
        ActivityMainBinding.inflate(layoutInflater)
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        NYCApp.component.inject(this)

        val navController = supportFragmentManager.findFragmentById(R.id.nav_container_nyc) as NavController
        setupActionBarWithNavController(navController)
    }
}