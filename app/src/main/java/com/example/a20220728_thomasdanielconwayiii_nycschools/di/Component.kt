package com.example.a20220728_thomasdanielconwayiii_nycschools.di

import com.example.a20220728_thomasdanielconwayiii_nycschools.MainActivity
import com.example.a20220728_thomasdanielconwayiii_nycschools.SchoolsFragment
import com.example.a20220728_thomasdanielconwayiii_nycschools.ScoresFragment
import com.example.a20220728_thomasdanielconwayiii_nycschools.utils.CommonFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [
    ApplicationModule::class,
    NetworkModule::class,
    ServiceRepositoryModule::class,
    ViewModelModule::class
])
interface Component {
    fun inject(mainActivity: MainActivity)
    fun inject(fragment: CommonFragment)
    fun inject(schoolsFragment: SchoolsFragment)
    fun inject(scoresFragment: ScoresFragment)
}