package com.example.a20220728_thomasdanielconwayiii_nycschools.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.a20220728_thomasdanielconwayiii_nycschools.service.ServiceRepository
import com.example.a20220728_thomasdanielconwayiii_nycschools.viewmodel.ViewModelSchool
import dagger.Binds
import dagger.Module
import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject
import javax.inject.Singleton

@Module
abstract class ViewModelModule {

    @Binds
    abstract fun providesViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}

@Singleton
class ViewModelFactory @Inject constructor(
    private val serviceRepository: ServiceRepository,
    private val ioScheduler: Scheduler,
    private val compositeDisposable: CompositeDisposable
) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T =
        ViewModelSchool(serviceRepository, ioScheduler, compositeDisposable) as T
}