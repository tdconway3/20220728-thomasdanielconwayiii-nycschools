package com.example.a20220728_thomasdanielconwayiii_nycschools.di

import android.app.Application

class NYCApp : Application() {

    override fun onCreate() {
        super.onCreate()
        component = DaggerComponent.builder()
            .applicationModule(ApplicationModule(this))
            .build()
    }

    companion object {
        lateinit var component: Component
    }
}