package com.example.a20220728_thomasdanielconwayiii_nycschools.di

import com.example.a20220728_thomasdanielconwayiii_nycschools.service.ServiceRepository
import com.example.a20220728_thomasdanielconwayiii_nycschools.service.ServiceRepositoryImpl
import dagger.Binds
import dagger.Module

@Module
abstract class ServiceRepositoryModule {

    @Binds
    abstract fun providesServiceRepository(serviceRepositoryImpl: ServiceRepositoryImpl): ServiceRepository
}