package com.example.a20220728_thomasdanielconwayiii_nycschools.model

import com.google.gson.annotations.SerializedName


data class SchoolsModel (
    @SerializedName("dbn"                              ) var dbn                            : String? = null,
    @SerializedName("school_name"                      ) var schoolName                     : String? = null,
    @SerializedName("overview_paragraph"               ) var overviewParagraph              : String? = null,
    @SerializedName("location"                         ) var location                       : String? = null,
    @SerializedName("phone_number"                     ) var phoneNumber                    : String? = null,
    @SerializedName("fax_number"                       ) var faxNumber                      : String? = null,
    @SerializedName("school_email"                     ) var schoolEmail                    : String? = null,
    @SerializedName("website"                          ) var website                        : String? = null,
    @SerializedName("finalgrades"                      ) var finalgrades                    : String? = null,
    @SerializedName("total_students"                   ) var totalStudents                  : String? = null,
    @SerializedName("primary_address_line_1"           ) var primaryAddressLine1            : String? = null,
    @SerializedName("city"                             ) var city                           : String? = null,
    @SerializedName("zip"                              ) var zip                            : String? = null,
    @SerializedName("state_code"                       ) var stateCode                      : String? = null,
    @SerializedName("latitude") var latitude                       : String? = null,
    @SerializedName("longitude") var longitude                      : String? = null,
)